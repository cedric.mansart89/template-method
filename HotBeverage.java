public abstract class HotBeverage {

final void prepare(){
    System.out.println("=== " + this.getClass().getSimpleName() + " ===");
    boilWater();
    browSomething();
    addSugar();
}

public void boilWater(){
    System.out.println("Boil water");
}

public void addSugar(){
    System.out.println("Add sugar");
}

abstract void browSomething();

}
